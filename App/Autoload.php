<?php

class Autoload
{
    public static function load($className)
    {

        $fileName = str_replace('\\','/', $className);
        $fileName .= ".php";

        include_once $fileName;
        if (class_exists($className)) {
            return true;
        }

        return false;
    }
}

spl_autoload_register("Autoload::load");
