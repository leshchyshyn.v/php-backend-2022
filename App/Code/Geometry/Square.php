<?php

namespace App\Code\Geometry;

use App\Code\Api\PerimeterOperationInterface as PerimeterOperationInterfaceAlias;
use App\Code\Api\SquareOperationInterface;
use App\Fixtures\SideNotSetException;

class Square extends Figure implements PerimeterOperationInterfaceAlias, SquareOperationInterface
{
    public function getPerimeter()
    {
        return $this->getA()*4;
    }

    public function getSquare()
    {
        return $this->getA()*$this->getA();
    }

    static function getStaticSquare($side = null)
    {
        if ($side) {
            return $side*$side;
        }
        throw new SideNotSetException("side is not valid");
    }
}