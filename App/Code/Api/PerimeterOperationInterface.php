<?php

namespace App\Code\Api;

interface PerimeterOperationInterface
{
    public function getPerimeter();
}